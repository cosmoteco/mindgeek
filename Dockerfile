FROM php:7.3-fpm

WORKDIR /app
COPY . /app


CMD php artisan serve --host=0.0.0.0 --port=8181
EXPOSE 8181
