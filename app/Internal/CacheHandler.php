<?php

namespace App\Internal;

use App\Internal\ToolService as ToolService;
use App\Internal\JsonProcessor as JsonProcessor;

use Illuminate\Support\Facades\Queue;
use App\Jobs\BeanstalkdJob;
use App\Jobs\ProcessImageJob;

class CacheHandler {
	public function cacheJson(string $url, string $content): string
	{
		$urlCacheKey = ToolService::hashString($url);

		app('redis')->set($urlCacheKey, $content);
		app('redis')->expire($urlCacheKey, JsonProcessor::JSON_CACHE_EXPIRE);

		return app('redis')->get($urlCacheKey);
	}

	public static function generateImageCacheKey(string $url, string $date): string
	{
		$urlHash = ToolService::hashString($url);
		$timeHash = ToolService::hashTime($date);

		return $urlHash . '_' . $timeHash;
	}

	public function cacheJsonImages(array $decodedJson)
	{
		foreach ($decodedJson as $key => $item) {
			// used for cache invalidation
			$lastUpdated = $item[JsonProcessor::JSON_LAST_UPDATED_KEY];

			$images = array_merge_recursive(
				$item[JsonProcessor::JSON_IMAGE_KEY[0]],
				$item[JsonProcessor::JSON_IMAGE_KEY[1]]
			);
			//die(dump($images));
                	Queue::push(new ProcessImageJob(['data' => ['images' => $images, 'lastUpdated' => $lastUpdated]]));
		}

	}

}

