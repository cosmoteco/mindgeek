<?php

namespace App\Internal;

class ToolService {
	public static function hashString(string $string): string {
		return md5($string);
	}

	public static function hashTime(string $date): string {
		return strtotime($date);
	}
}

