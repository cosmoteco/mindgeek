<?php

namespace App\Internal;

use App\Internal\CacheHandler as Cache;
use App\Internal\ToolService as ToolService;

class JsonProcessor {
	const JSON_URL = 'https://mgtechtest.blob.core.windows.net/files/showcase.json';
	const JSON_CACHE_EXPIRE = 3600; // arbitrary added JSON expire time: 1 hour
	const JSON_LAST_UPDATED_KEY = 'lastUpdated';
	const JSON_IMAGE_KEY = [
		'cardImages',
		'keyArtImages',
	];
	const JSON_IMAGE_CONTENT_TYPE = [
		'image/jpeg',
		'image/gif',
		'image/png',
	];

	private $cacheHandler;

	public function __construct($cacheHandler)
	{
		$this->cacheHandler = $cacheHandler;
	}

	public static function getJson(string $url): string
	{
		return file_get_contents($url);
	}

	public function processJson(array $decodedJson): array
	{	
		//$cacheKey = ToolService::hashString(serialize($decodedJson));

		//if (app('redis')->exists($cacheKey)) {
		//	return unserialize(app('redis')->get($cacheKey));
		//}

		$this->cacheHandler->cacheJsonImages($decodedJson);
		$decodedJson = $this->updateImageUrl($decodedJson);
		//app('redis')->set($cacheKey, serialize($decodedJson));
		return $decodedJson;
	}

	public static function file_get_contents_curl(string $url): array
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		$data = curl_exec($ch);

		$content_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);

		curl_close($ch);

		if (in_array($content_type, self::JSON_IMAGE_CONTENT_TYPE)) {
			return [
				'extension' => self::getFileExtension($url),
				'content' => $data,
			];
		}

		// only save image content
		return [
			'extension' => self::getFileExtension($url),
			'content' => null,
		];
	}

	public function updateImageUrl(array $decodedJson): array
	{
		foreach ($decodedJson as &$item) {
			$cardImages = &$item[JsonProcessor::JSON_IMAGE_KEY[0]];
			foreach ($cardImages as &$image) {
				$imageKey = $this->cacheHandler->generateImageCacheKey(
					$image['url'],
					$item[self::JSON_LAST_UPDATED_KEY]
				);
				if (app('redis')->exists($imageKey)) {
					$image['cache'] = app('redis')->get($imageKey);
				}
			}

			$keyArtImages = &$item[JsonProcessor::JSON_IMAGE_KEY[1]];
			foreach ($keyArtImages as &$image) {
				$imageKey = $this->cacheHandler->generateImageCacheKey(
					$image['url'],
					$item[self::JSON_LAST_UPDATED_KEY]
				);

				if (app('redis')->exists($imageKey)) {
					$image['cache'] = app('redis')->get($imageKey);
				}
			}
		}
		return $decodedJson;
	}

	public static function getDecodedContent(string $jsonData): array
	{
		$escapedJsonData = utf8_encode($jsonData);

		$jsonDecodedData = json_decode($escapedJsonData, true);

		return $jsonDecodedData;
	}

	private static function getFileExtension(string $url): string
	{
		$ext = pathinfo($url, PATHINFO_EXTENSION);

		return '.' . $ext;
	}
}

