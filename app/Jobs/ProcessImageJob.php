<?php
namespace App\Jobs;

use App\Jobs\Job;
use Log;
use Illuminate\Http\Request;

use App\Internal\JsonProcessor as JsonProcessor;
use App\Internal\CacheHandler as CacheHandler;

class ProcessImageJob extends Job {
	const IMAGE_CONTENT_TYPE = [
		'image/jpeg',
		'image/gif',
		'image/png',
	];

    public $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data['data'];
    }

    /**
     * Execute the job.
     *
     * @param
     * @return void
     */
    public function handle() {
	$images = $this->data['images'];
	$dateModified = $this->data['lastUpdated'];
	foreach ($images as $key => $image) {
		$imageCacheKey = CacheHandler::generateImageCacheKey($image['url'], $dateModified);
		
		if (app('redis')->exists($imageCacheKey)) {
			continue;
                }
		
		$result = static::file_get_contents_curl($image['url']);
		$cacheFilePath = static::saveImage($imageCacheKey, $result['extension'], $result['content']);
		
		app('redis')->set($imageCacheKey, $cacheFilePath);
	}
    }
    
    private static function saveImage(string $name, string $extension, $content): string
    {
	    $path = env('CACHE_IMAGE_FOLDER') . $name . $extension;
	    if (file_exists($path)) {
		    return $path;
	    }
	    
	    file_put_contents($path, $content);
	    return $path;
    }


    	public static function file_get_contents_curl(string $url): array
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		$data = curl_exec($ch);

		$content_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);

		curl_close($ch);

		if (in_array($content_type, static::IMAGE_CONTENT_TYPE)) {
			return [
				'extension' => static::getFileExtension($url),
				'content' => $data,
			];
		}

		return [
			'extension' => self::getFileExtension($url),
			'content' => null,
		];
	}

	private static function getFileExtension(string $url): string
	{
		$ext = pathinfo($url, PATHINFO_EXTENSION);

		return '.' . $ext;
	}
}
