<?php

namespace App\Http\Controllers;
	
use Illuminate\Support\Facades\Queue;
use App\Jobs\BeanstalkdJob;
use App\Jobs\ProcessImageJob;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use App\Internal\JsonProcessor;
use App\Internal\CacheHandler;
use App\Internal\ToolService as ToolService;

class SampleController extends Controller
{

	const JSON_URL = JsonProcessor::JSON_URL;
	const ITEMS_PER_PAGE =1;

	private $jsonProcessor;
	private $cacheHandler;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    	public function __construct($cache = null, $json = null)
	{
		$this->cacheHandler = $cache;
		$this->jsonProcessor = $json;
	}

    public function publish() {
	for($i=0; $i<=1000; $i++) {
        	Queue::push(new BeanstalkdJob(array('queue' => $i)));
			echo "successfully push";
	}

	for($i=0; $i<=1000; $i++) {
                Queue::push(new ProcessImageJob(['queue' => [$i=>'test'.$i]]));
        }

    }

	public function demo(Request $request)
	{
		$currentPage = $request->input('page');

		try {
			$cachedJson = '';

			if (app('redis')->exists(ToolService::hashString(self::JSON_URL))) {
				$cachedJson = app('redis')->get(ToolService::hashString(self::JSON_URL));
			}

			if (!app('redis')->exists(ToolService::hashString(self::JSON_URL))) {
				$jsonData = JsonProcessor::getJson(static::JSON_URL); // get JSON from url
				$cachedJson = $this->getCacheHandler()->cacheJson(static::JSON_URL, $jsonData);

			}

			$decodedJson = JsonProcessor::getDecodedContent($cachedJson);

			//send all content for caching
			$processedJson = $this->getJsonProcessor()->processJson($decodedJson);
			
			// used to only create cache for current page
                        $paginator = self::paginate($decodedJson, $currentPage);
                        //die(dump($paginator->items()));
			
			$data = $this->getJsonProcessor()->processJson($paginator->items());
			//die(dump($processedJson));
		} catch (\Exception $e) {
			return response($e->getMessage(), 503); // Service Unavailable
		}
		
		return view(
			'show',
			[
				'data' => $data,
				//'data' => $paginator->items(),
				'paginator' => $paginator,
			]
		);
	}

	public static function paginate($processedJson, $currentPage)
	{
		return new Paginator(
			self::getCurrentItems($processedJson, $currentPage),
			count($processedJson),
			self::ITEMS_PER_PAGE,
			$currentPage
		);
	}

	protected function getJsonProcessor()
	{
		if (null === $this->jsonProcessor) {
			$this->jsonProcessor = new JsonProcessor($this->getCacheHandler());
		}
		return $this->jsonProcessor;
	}

	protected function getCacheHandler()
	{
		if (null === $this->cacheHandler) {
			$this->cacheHandler = new CacheHandler();
		}

		return $this->cacheHandler;
	}

	private static function getCurrentItems($items, $currentPage): array
	{
		// Get current items calculated with per page and current page
		return array_slice($items, self::ITEMS_PER_PAGE * ($currentPage - 1), self::ITEMS_PER_PAGE);
	}

    //
}
