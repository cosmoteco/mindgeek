<?php $keys = array_keys($item); ?>
<ul class="parent-list">
<?php foreach ($keys as $key => $name): ?>
<?php
		if ('cardImages' === $name) {
			include ('carousel.php');
			continue;
		}
		if ('keyArtImages' === $name) {
			include ('carousel.php');
			continue;
		}
		try {
			if (is_array($item[$name])) {

				include 'loop.php';
			} else {
?>
	<li class="item"><label class="label"><?php echo $name; ?>:</label>&nbsp;<?php echo $item[$name]; ?></li>
<?php
			}
		} catch (Exception $e) {
			// PHP7: handle the error message $e->getMessage();
		}
	endforeach;
?>
</ul>
