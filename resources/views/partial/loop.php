<li class="item">
	<label class="label"><?php echo $name; ?>:&nbsp;</label><br/ >
<?php $keys = $item[$name]; ?>
	<ul class="sub-list">
	<?php foreach ($keys as $key => $name): ?>
		<?php
			if (is_array($name)) {
				include 'second-loop.php';

				continue;
			}
		?>
		<li class="item"><label class="label"><?php if (is_string($key)) echo $key, ':&nbsp;'; ?></label><?php echo $name; ?></li>
	<?php endforeach; ?>
	</ul>
</li>
