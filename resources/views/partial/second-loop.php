<?php $subKeys = array_keys($name); ?>
<?php foreach ($subKeys as $value): ?>
	<?php
		if (is_array($name[$value])) {
			include 'third-loop.php';

			continue;
		}
	?>
	<li class="item"><label class="label"><?php echo $value; ?>:</label>&nbsp;<?php echo $name[$value]; ?></li>
<?php endforeach; ?>
